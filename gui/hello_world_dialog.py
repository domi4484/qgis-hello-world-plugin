# -*- coding: utf-8 -*-

import os

from qgis.PyQt.QtCore import (
    pyqtSlot,
    Qt
)
from qgis.PyQt.QtGui import QIcon
from qgis.PyQt.QtWidgets import (
    QAction,
    QDialog
)
from qgis.PyQt.uic import loadUiType
from qgis.core import (
    Qgis
)


WidgetUi, _ = loadUiType(os.path.join(os.path.dirname(__file__), '../ui/hello_world_dialog.ui'))

class HelloWorldDialog(QDialog, WidgetUi):

    def __init__(self, interface):
        super().__init__()
        self.setupUi(self)
        self.interface = interface

        # Actions
        self.actionSayHello = QAction(QIcon(":/images/themes/default/mActionToggleEditing.svg"),
                                           self.tr("Say hello!"))

        # Tool buttons
        self.mSayHelloToolButton.setDefaultAction(self.actionSayHello)
        self.mSayHelloToolButton.setToolButtonStyle(Qt.ToolButtonTextBesideIcon)

        # Signal slots
        self.actionSayHello.triggered.connect(self.sayHello)


    @pyqtSlot(bool)
    def sayHello(self):
        print('Hello world')

        from qgis.core import Qgis
        self.interface.messageBar().pushMessage("Error", "Hello World!", level=Qgis.Success)
